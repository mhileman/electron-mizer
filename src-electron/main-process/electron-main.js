import { app, BrowserWindow, Menu } from 'electron'

/**
 * Set `__statics` path to static files in production;
 * The reason we are setting it here is that the path needs to be evaluated at runtime
 */
if (process.env.PROD) {
  global.__statics = require('path').join(__dirname, 'statics').replace(/\\/g, '\\\\')
}

let mainWindow

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    width: 800,
    height: 800,
    useContentSize: true
  })

  mainWindow.loadURL(process.env.APP_URL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  Menu.setApplicationMenu(Menu.buildFromTemplate(menuTemplate))
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

// Application main menu
var menuTemplate = [
{
  label: "Electron-Mizer",
  submenu: [{
    label: "About Application",
    selector: "orderFrontStandardAboutPanel:"
  },
  {
    label: "Hide",
    accelerator: "Command+H",
    selector: "hide:"
  },
  {
    type: "separator"
  },
  {
    label: "Quit",
    accelerator: "Command+Q",
    click: function() { app.quit(); }
  }]
},
{
  label: "Edit",
  submenu: [
  {
    label: "Copy",
    accelerator: "CmdOrCtrl+C",
    role: "copy"
  },
  {
    label: "Paste",
    accelerator: "CmdOrCtrl+V",
    role: "paste"
  },
  {
    label: "Select All",
    accelerator: "CmdOrCtrl+A",
    role: "selectall"
  }]
},
{
  label: "View",
  submenu: [
  {
    label: "Console",
    accelerator: "CmdOrCtrl+I",
    click: function() {
      mainWindow.webContents.openDevTools(['undocked'])
    }
    // role: "toggledevtools"
  },
  {
    label: "Reload",
    accelerator: "CmdOrCtrl+R",
    role: "reload"
  }]
}]
