# Electron-Mizer

POC integration of [XNAT Mizer](https://bitbucket.org/xnatdcm/mizer-app) with Electron.

## Develop
```sh
$ npm install
$ npm run dev
```

## Build for Mac and Windows
```sh
$ npm run build
```
